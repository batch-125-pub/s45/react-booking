import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css';

/*component*/

//do not forget to place ./ (currect directory as it will error)
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';



ReactDOM.render(
  <App/>,
  document.getElementById('root'));

//everything that will be rendered in the area of <h1>Hello World</h1>, will be rendered in the target element document.getElementById.('root')

//<div><h1>Hello World</h1><h2>hello po</h2></div> -> JSX elements or Javascript xml elements -> extension of Javascript; allows to write HTML elements in Javascript files


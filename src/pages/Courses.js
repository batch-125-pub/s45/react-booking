
import React, {useState, useEffect, useContext} from 'react';

/*react-bootstrap component*/
import {Container} from 'react-bootstrap';

/*components*/
import Course from './../components/Course';

/*mock data*/
// import courses from './../mock-data/courses';

import AdminView from './../components/AdminView';
import UserView from './../components/UserView';

/*context*/

import UserContext from './../UserContext';

export default function Courses(){

	const [courses, setCourses] = useState([]);

	const {user} = useContext(UserContext);

	let token = localStorage.getItem("token");

	let CourseCards;

	//fetch
	const fetchData = () => {
		fetch('https://pacific-falls-33363.herokuapp.com/api/products/all-products', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		}).then(result=>result.json())
		.then(result=>{
			console.log(result);
			setCourses(result);
		})	
	}

	useEffect(()=>{
		fetchData();
	}, [])

	//display courses using map

	
	// CourseCards = courses.map((course)=>{
	// 	// console.log(course)
	// 	return <Course course = {course}/>

	// 	//passing (course placeholder) an argument to Course component

	// 	//courses.map -> mapping the array of objects in courses.js
	// })

	// console.log("list of courses", courses);
	return (
		<Container fluid>
			{ (user.isAdmin == "true") ?
				<AdminView courseData = {courses} fetchData={fetchData}/>
				:
				<UserView courseData = {courses}/>
			}
		</Container>
	)
}
import React, {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';

/*Context*/
import UserContext from './../UserContext';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	//define the user and setUser state, to use setUser method
	const{user, setUser} = useContext(UserContext);

	useEffect( ()=>{
		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password, verifyPassword]);

	function login(e){
		e.preventDefault();

		fetch('https://pacific-falls-33363.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(result=>result.json())
		.then((result)=>{
			// console.log("login result:", result);
			// alert('login Successful');

			if(typeof result.access !== "undefined"){
				localStorage.setItem('token', result.access);

				userDetails(result.access);
			}
		})


		const userDetails = (token) => {
			fetch('https://pacific-falls-33363.herokuapp.com/api/users/get-profile', {
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}).then(result => result.json())
			.then(result => {
				console.log(result); //whole user object
				
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				});
				console.log("user val:", user);
				console.log("isadmin val", result.isAdmin);
				localStorage.setItem('isAdmin', result.isAdmin);

			})
		}

		// alert('Login Successful');

		//update the user using email
		setUser({email: email});

		// save email to local storage
		localStorage.setItem('email', email);
		


		// console.log(user);
		setEmail('');
		setPassword('');
		// setVerifyPassword('');
	}

	// if(user.email !== null){
	// 	return <Redirect to = "/" />
	// }

	return (
		(user.id !== null) ? 
			<Redirect to="/"/>
		: 
		<Container>
			<h1 className = "text-center mt-4">Login</h1>
			<Form onSubmit={(e)=>login(e)} className ="p-5">
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value = {email} onChange={(e)=> setEmail(e.target.value)}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value = {password} onChange={(e)=>{setPassword(e.target.value)}}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formVerifyPassword">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" placeholder="Verify Password" value = {verifyPassword} onChange={(e)=>{setVerifyPassword(e.target.value)}}/>
			  </Form.Group>
			  <Button variant="primary" type="submit" disabled = {isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}
import React, {useContext, useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {
	Container,
	Card,
	Button
} from 'react-bootstrap';

import Swal from 'sweetalert2'

export default function SpecificCourse(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	let token = localStorage.getItem('token');

	let history = useHistory();

	const {user} = useContext(UserContext);

	const {courseId} = useParams();

	useEffect(()=> {

		fetch(`https://pacific-falls-33363.herokuapp.com/api/products/${courseId}`, {
			headers:{ //GET method is declared by default
				"Authorization": "application/json"
			}	
		}).then(result=>result.json())
		.then(result=>{
			console.log(result);

			setName(result.name);
			setDescription(result.description);
			setPrice(result.price);
		})
	}, [])

	const enroll = () => {
		fetch(`https://pacific-falls-33363.herokuapp.com/api/orders/${}/add-to-cart`,{
			method: "POST",
			headers:{
				"Authorization" : `Bearer ${token}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				courseId: courseId
			})
		}).then(result=>result.json())
		.then(result=>{

			if(result=== true){

				Swal.fire({

					title: "Success",
					icon: "success",
					text: "Enrolled Successfully"
				})

				history.push('/courses');
			}
		})
	}

	return(
		<Container>
			<Card.Header>
				<h4> 
				{/*coursename*/}
				{name}
				</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>
					{/*course desc*/}
					{description}
				</Card.Text>
				<h6>
				Price: Php 
					{/*courseprice*/}
					{price}
				</h6>
			</Card.Body>
			<Card.Footer>
				{
					(user.id !== null) ?
					<Button variant="primary" onSubmit={()=>enroll()}>Enroll</Button>
					:
					<Link className = "btn btn-danger" to="/login">Login to Enroll</Link>
				}
			</Card.Footer>
		</Container>
	)
}
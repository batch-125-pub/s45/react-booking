import React, {useState, useEffect, useContext} from 'react';

import {useHistory} from 'react-router-dom';

import {Container, Form, Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';

import UserContext from './../UserContext';

import Swal from 'sweetalert2';



export default function Register(){

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user} = useContext(UserContext);

	let history = useHistory();

	console.log(user);

	useEffect( ()=>{
		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password, verifyPassword]);

	function register(e){
		e.preventDefault();

		// alert('Registration Successful, you may now login');

		fetch('https://pacific-falls-33363.herokuapp.com/api/users/check-email', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		}).then( result => result.json())
		.then(result => {
			// console.log(result);

				if(result === true){
					Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please choose another email"
				})

				} else {

					fetch('https://pacific-falls-33363.herokuapp.com/api/users/register', {
						method: "POST",
						headers: {
							"Content-Type" : "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password
						})
					}).then(result=>result.json())
					.then((result)=>{
						// console.log(result);
						if(result === true){
							Swal.fire({
								title: "Registration Successful",
								icon: "success",
								text: "Welcome to  Course Booking"
							})

							history.push('/login');
						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})

						}
						
					})

			}
		})

		setEmail('');
		setPassword('');
		setVerifyPassword('');

	}

	return (
		
		(user.id !== null) ?
			<Redirect to = "/" />
		:
		
		<Container>
			<h1 className = "text-center mt-4">Register</h1>
			<Form onSubmit={register} className ="p-5">
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>First Name:</Form.Label>
			    <Form.Control type="text" placeholder="Enter First Name:" value = {firstName} onChange={(e)=> setFirstName(e.target.value)}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Last Name:</Form.Label>
			    <Form.Control type="text" placeholder="Enter Last Name:" value = {lastName} onChange={(e)=> setLastName(e.target.value)}/>
			  </Form.Group>
			   <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Mobile Number:</Form.Label>
			    <Form.Control type="text" placeholder="Enter Mobile Number:" value = {mobileNo} onChange={(e)=> setMobileNo(e.target.value)}/>
			  </Form.Group>
			   <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email:</Form.Label>
			    <Form.Control type="email" placeholder="Enter Email:" value = {email} onChange={(e)=> setEmail(e.target.value)}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value = {password} onChange={(e)=>{setPassword(e.target.value)}}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formVerifyPassword">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" placeholder="Verify Password" value = {verifyPassword} onChange={(e)=>{setVerifyPassword(e.target.value)}}/>
			  </Form.Group>
			  <Button variant="primary" type="submit" disabled = {isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}
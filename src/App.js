import React, {useState, useEffect} from 'react';

import {
	BrowserRouter,
	Route,
	Switch
} from 'react-router-dom';

/*Context*/
import UserContext from './UserContext';


/*components*/
import AppNavbar from './components/AppNavbar'; 
// import CourseCard from './components/CourseCard';
// import Welcome from './components/Welcome';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NullPath from './components/NullPath';
// import Counter from './components/Counter'

import SpecificCourse from './pages/SpecificCourse';

export default function App(){

	const [user, setUser] = useState({
								id: null,
								isAdmin: null
							});

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		});
	}

	useEffect(()=>{

		let token = localStorage.getItem('token');
		fetch('https://pacific-falls-33363.herokuapp.com/api/users/get-profile',
		{	
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		}).then(result=>result.json())
		.then(result=>{
			// console.log(result);
			//result is an object or document of the current or logged user details
			if(typeof result._id !== "undefined"){
					setUser({
						id: result._id,
						isAdmin: result.isAdmin
					})

					// console.log("logged in user", user);
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	})

	return (

	/*	<Fragment>
			<AppNavbar/>
			<Home/>}
			<CourseCard/>
			<Courses/>
			<Counter/>
			<Register/>
			<Login/>
		</Fragment>
	*/

	<UserContext.Provider value={{user, setUser, unsetUser}}>
		<BrowserRouter>
			<AppNavbar/>
			<Switch>
				<Route exact path="/" component={Home}/>
				<Route exact path="/courses" component={Courses}/>
				<Route exact path="/register" component={Register}/>
				<Route exact path="/login" component={Login}/>
				<Route exact path = "/courses/:courseId" component={SpecificCourse}/>
				<Route component={NullPath}/>
			</Switch>
		</BrowserRouter>
	</UserContext.Provider>
	)
}
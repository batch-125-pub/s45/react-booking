
import React from 'react';

import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';

export default function Banner(){
	return (
		/*
		<div className = "container-fluid">
			<div className = "row">
				<div className = "col-10 col-md-8">
					<div className = "jumbotron">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere</p>
						<button className = "btn btn-primary">Enroll</button>
					</div>
				</div>
			</div>
		</div>
		*/

		<Container fluid>
			<Row className = "g-0">
				<Col className="p-0 m-0">
					<Jumbotron fluid className="px-3">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere</p>
						<Button variant="primary">Enroll</Button>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)

}
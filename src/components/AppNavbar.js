import React, {Fragment, useContext} from 'react';
// import {Redirect} from 'react-router-dom';
/*importing react bootstrap*/

// other way of importing
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
//or type in the following (without specifically locating inside Navbar or Nav):

import {Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink, useHistory} from 'react-router-dom'; //imports from react router dom
import UserContext from './../UserContext';

/*app navbar*/

export default function AppNavbar(){

  // console.log(props);

  // let user = props.user;

  const {user, unsetUser} = useContext(UserContext);

//useHistory is a react-router-dom hook
  let history = useHistory();

  const logout = () => {
    unsetUser();
    history.push('/login');
  }

  let leftNav = (user.id === null) ? (
      <Fragment>
        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        <Nav.Link as={NavLink} to = "/login">Login</Nav.Link>
      </Fragment>
    ) : (
      <Fragment>
          <Nav.Link onClick = {logout} >Logout</Nav.Link>
      </Fragment>
    )


  return (
    <Navbar variant="dark" bg="dark" expand="lg">
      <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to = "/courses">Courses</Nav.Link>
        </Nav>
        <Nav>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar> 

     
    )
}
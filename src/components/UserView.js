import React, {useState, useEffect} from 'react';

import {
	Container
} from 'react-bootstrap'

import Course from './Course';

export default function UserView({courseData}){
	// let courseArr;
	// console.log("this is courseData", courseData);

	const [courses, setCourses] = useState([]);

	useEffect(()=>{

		const courseArr = courseData.map((course)=>{
			// console.log(course);

			if(course.isActive === true){
				console.log("every course", course)
				return <Course key= {course._id} courseProp = {course}/>
			} else {
				return null;
			}
		})

		setCourses(courseArr);

	}, [courseData]); //variable or value being manipulated everytime the useEffect is triggered

	return (
		<Container>
			{/*display courses*/}
			{courses}
		</Container>
	)
}
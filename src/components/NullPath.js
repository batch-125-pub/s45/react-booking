import React from 'react';

import {Container} from 'react-bootstrap';

export default function NullPath(){

	return(
		<Container>
			<h1 className="mt-5">404 - Not Found</h1>

			<p>The Page you are looking for cannot be found</p>

			<a href="./">Back Home</a>
		</Container>

	)

}
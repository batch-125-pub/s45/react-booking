import React, {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types'; //validation or sends and error in the console if a missing data or value is not entered or present in the mock data. (app might still work despite the error - will only show the error message in the console)

export default function Course(courseProp){
	console.log("this is a courseprop", courseProp);
	// let courseData;
	const {_id, name, description, price} = courseProp;
	// console.log(props.cor1.name)
	
	// let course = props.course;
	// console.log(props);
	//States
	//syntax:
	//[state, setState] = useState()
	// const [count, setCount] = useState(10);
	// const [isDisabled, setIsDisabled] = useState(false);

	// function enroll(){
	// 	if(count === 0){
	// 		//setIsDisabled(true);
	// 		alert("No seats available");
	// 	} else {
	// 		setCount(count - 1);


	// 	}
	// }

	// useEffect( ()=>{

	// 	if(count === 0){
	// 		document.title = `You clicked ${count} times`;
	// 		setIsDisabled(true);

	// 	}

	// }, [count]);


	return (
		<Card>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Text>
		    <p> Description: </p>
		    <p>{description}</p>
		    <p>Price:</p>
		    <p>{price}</p>
		    {/*<h5>{count} Enrollees</h5>
		    <p></p>
		    </Card.Text>
		    <Button variant="primary" onClick = {enroll} disabled={isDisabled}>Enroll</Button>*/}

		    <Link className = "btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>	

	)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}